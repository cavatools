G ?= riscv64
H ?= amd64


ifneq ($G,riscv64)
ifneq ($G,ppc)
ifneq ($G,ppc64)
$(error G: allowed values: riscv64, ppc, ppc64, not '$G')
endif
endif
endif


ifneq ($H,amd64)
$(error H: allowed values: amd64, not '$H')
endif
